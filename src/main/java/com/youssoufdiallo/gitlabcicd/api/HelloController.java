package com.youssoufdiallo.gitlabcicd.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/")
    public String hello(){
        return "Hello world!";
    }
    @GetMapping("/{name}")
    public String helloQuery(@PathVariable String name){
        return "Hello "+name;

    }
}



